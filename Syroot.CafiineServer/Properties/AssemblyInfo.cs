﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Cafiine Server")]
[assembly: AssemblyDescription("Syroot Cafiine Server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot / Chadsoft")]
[assembly: AssemblyProduct("Cafiine Server")]
[assembly: AssemblyCopyright("Copyright © Syroot 2016 / ChadSoft 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.8.0.0")]
[assembly: AssemblyFileVersion("0.8.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("fc022709-becf-498f-9a2a-dc3543457b0d")]
